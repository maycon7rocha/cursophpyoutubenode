<?php

class Produtos {
    public $nome;
    public $valor;

    function __construct($nome, $valor)
    {
        $this->nome = $nome;
        $this->valor = $valor; 
    }
}

class Carrinho {
    public $produtos;

    public function adicionar(Produtos $produto){
        $this->produtos[] = $produto;
    }

    public function exibe() {
        foreach ($this->produtos as $produto) {
            echo $produto->nome;
            echo " R$ ".$produto->valor;
            echo "<hr>";
        }
    }
}


$produto1 = new Produtos("Notebook", "1500");
$produto2 = new Produtos("Mouse", "302");


$carrinho = new Carrinho();
$carrinho->adicionar($produto1);
$carrinho->adicionar($produto2);

$carrinho->exibe();