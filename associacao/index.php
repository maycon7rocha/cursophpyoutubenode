<?php

class Pedido {
    public $numero;
    public $cliente;
}

class Cliente {
    public $nome;
    public $endereco;
}

$cliente = new Cliente();
$cliente->nome = "Maycon Ferreira Rocha";
$cliente->endereco = "Rua xxx, número 177";


$pedido = new Pedido();
$pedido->numero = "244";

$pedido->cliente = $cliente;

var_dump($pedido);
echo "<hr>";

$dados = array(
    'numero' => $pedido->numero,
    'nome_cliente' => $pedido->cliente->nome,
    'end_cliente' => $pedido->cliente->endereco,
);

var_dump($dados);