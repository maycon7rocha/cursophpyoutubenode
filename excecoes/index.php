<?php

class Newsletter {

    public function cadastrarEmail($email){
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)):
            throw new Exception("Este email é inválido", 1);
        else:
            echo "Email cadastrado com sucesso";
        endif;
    }
}

$newsletter = new Newsletter();
try {
    $newsletter->cadastrarEmail("contato@");

} catch(Exception $e){
    var_dump($e);
    echo "<hr>";
    echo $e->getMessage();
    echo "<hr>";
    echo $e->getCode();
    echo "<hr>";
    echo $e->getLine();
    echo "<hr>";
    echo $e->getFile();
}