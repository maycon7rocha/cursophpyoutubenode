<?php

class Pessoa {
    private $dados = array(); 

    // metodo magico set
    public function __set($nome, $valor) {
        $this->dados[$nome] = $valor;
    }

    // metodo magico get
    public function __get($nome){
        var_dump($this->dados);
        return $this->dados[$nome];
    }

    // metodo tostring
    public function __toString()
    {
        return " Tentei imprimir o objeto ".$this->dados["nome"];
    }

    // metodo invoke
    public function __invoke()
    {
        return "Objeto como função";
    }
}

$pessoa = new Pessoa();
$pessoa->nome = "Danilo";
$pessoa->idade = 50;
$pessoa->sexo = "M";

echo $pessoa->nome;

echo $pessoa; // tostring

echo $pessoa(); // invoke pessoa como se fosse uma pessoa