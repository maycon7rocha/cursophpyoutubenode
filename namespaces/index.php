<?php

require "classes/produto.php";
require "models/produto.php";

use classes\Produto as ProductClasses;
use models\Produto as ProductModels;

$produto = new ProductClasses();
$produto->mostrarDetalhes();

echo "<hr>";

$produto = new ProductModels();
$produto->mostrarDetalhes();