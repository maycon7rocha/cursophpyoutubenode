<?php
require_once 'vendor/autoload.php';

use \App\Model\Produto;

$produto = new Produto();
$produto->setId(4);
$produto->setNome('Notebook DELL Inspira preto');
$produto->setDescricao('i7, 16GB');

// var_dump($produto);

$produtoDao = new \App\Model\ProdutoDao();
// $produtoDao->create($produto);
$produtoDao->update($produto);

// $produtoDao->delete(1);

// $produtos = $produtoDao->read();
// var_dump($produtos);

foreach ($produtoDao->read() as $produto) {
    echo $produto['nome']."<br>".$produto['descricao'].'<hr>';
}
