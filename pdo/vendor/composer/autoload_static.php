<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitdda3e68e903034f2c20f133f296633ea
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/App',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitdda3e68e903034f2c20f133f296633ea::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitdda3e68e903034f2c20f133f296633ea::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitdda3e68e903034f2c20f133f296633ea::$classMap;

        }, null, ClassLoader::class);
    }
}
